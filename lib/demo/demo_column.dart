import 'package:flutter/material.dart';

class DemoColumn extends StatelessWidget {
  const DemoColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Demo Column'),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            color: Colors.blue,
            width: 30,
            height: 30,
          ),
          Container(
            color: Colors.blue,
            width: 30,
            height: 30,
          )
        ],
      ),
    );
  }
}
