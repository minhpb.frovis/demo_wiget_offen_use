import 'package:flutter/material.dart';

class DemoStack extends StatelessWidget {
  const DemoStack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('"Demo Stack"'),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Container(
        height: 300,
        width: 300,
        child: Stack(
          children: [
            Container(
              alignment: Alignment.bottomCenter,
               height: 200,
              width: 200,
              child: const Text('widget 1'),
              color: Colors.red,
            ),
            Container(
               height: 100,
              width: 100,
              child: const Text('widget 2'),
              color: Colors.yellow,
            )
          ],
        ),
        decoration: BoxDecoration(
            color: Colors.blue, borderRadius: BorderRadius.circular(10)),
      ),
    );
  }
}
