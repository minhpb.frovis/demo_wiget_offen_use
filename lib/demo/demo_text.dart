import 'package:flutter/material.dart';

class DemoText extends StatelessWidget {
  const DemoText({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('"Demo Text"'),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: const [
          Text('text'),
          Text('text', style: TextStyle(fontSize: 18)),
          Text('text',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w200)),
          Text('text',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w200,
                  fontStyle: FontStyle.italic)),
        ],
      ),
    );
  }
}
