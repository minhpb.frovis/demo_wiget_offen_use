import 'package:flutter/material.dart';

class DemoRow extends StatelessWidget {
  const DemoRow({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('"Demo Row"'),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: Colors.blue,
            width: 30,
            height: 30,
          ),
          Container(
            color: Colors.blue,
            width: 30,
            height: 30,
          )
        ],
      ),
    );
  }
}
