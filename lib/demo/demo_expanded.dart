import 'package:flutter/material.dart';

class DemoExpanded extends StatelessWidget {
  const DemoExpanded({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('"Demo expanded"'),
        centerTitle: true,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: Colors.blue,
            width: 300,
            height: 300,
            child: Row(
              children: [
                Container(
                  color: Colors.red,
                  width: 100,
                  height: 30,
                  child: const Text('chưa có expaned'),
                )
              ],
            ),
          ),
          Container(
            color: Colors.blue,
            width: 300,
            height: 300,
            child: Row(
              children: [
                Container(
                  color: Colors.red,
                  width: 100,
                  height: 30,
                  child: const Text('có expaned'),
                ),
                Expanded(
                    child: Container(
                  color: Colors.yellow,
                  width: 50,
                  height: 30,
                  child: const Text('đây là expaned'),
                ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
