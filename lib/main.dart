import 'package:flutter/material.dart';
import 'package:flutter_fvm/demo/demo_column.dart';
import 'package:flutter_fvm/demo/demo_container.dart';
import 'package:flutter_fvm/demo/demo_expanded.dart';
import 'package:flutter_fvm/demo/demo_row.dart';
import 'package:flutter_fvm/demo/demo_stack.dart';
import 'package:flutter_fvm/demo/demo_text.dart';
import 'package:flutter_fvm/home_screen.dart';

void main() {
  runApp(const App());
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(initialRoute: '/', routes: {
      '/': (context) => const HomeScreen(),
      '/Row': (context) => const DemoRow(),
      '/Column': (context) => const DemoColumn(),
      '/Text': (context) => const DemoText(),
      '/Container': (context) => const DemoContainer(),
      '/Stack': (context) => const DemoStack(),
      '/Expanded': (context) => const DemoExpanded(),
    });
  }
}