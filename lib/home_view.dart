import 'package:flutter/material.dart';
import 'package:flutter_fvm/my_widget.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<MyWidget> datas = [
    MyWidget("Row", "đây là row"),
    MyWidget("Column", "đây là Column"),
    MyWidget("Text", "đây là row"),
    MyWidget("Container", "đây là Text"),
    MyWidget("Stack", "đây là Stack"),
    MyWidget("Expanded", "đây là Expanded")
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: datas.length,
      itemBuilder: (BuildContext context, int index) {
        return Item(
          name: datas[index].name,
          content: datas[index].content,
        );
      },
      separatorBuilder: (_, index) {
        return const Divider(height: 2,color: Colors.black,);
      },
    );
  }
}

class Item extends StatelessWidget {
  final String? name;
  final String? content;

  const Item({Key? key, this.name, this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: SizedBox(
        height: 50,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name ?? '',
                style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
              Text(
                content ?? '',
                style: const TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
              )
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.of(context).pushNamed('/$name');
      },
    );
  }
}
