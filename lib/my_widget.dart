class MyWidget {
  String name;
  String content;


  MyWidget(
    this.name,
    this.content,
  );

  
  String get getName => name;

  set setName(String name) => name = name;

  get getContent => content;

  set setContent(content) => content = content;
}
