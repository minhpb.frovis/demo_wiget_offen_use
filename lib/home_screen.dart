import 'package:flutter/material.dart';
import 'package:flutter_fvm/home_view.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('List wiget offen used '),centerTitle: true,),
      body: const HomeView(),
    );
  }
}
